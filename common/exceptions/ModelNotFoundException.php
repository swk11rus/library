<?php

namespace common\exceptions;

class ModelNotFoundException extends \Exception
{
}
