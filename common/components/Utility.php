<?php

namespace common\components;

use common\models\User;
use Yii;

class Utility
{
    public static function getDateNow($format = 'Y-m-d H:i:s'): string
    {
        $date = new \DateTime('now');
        return $date->format($format);
    }

    public static function getModelErrorsString(\yii\base\Model $model): string
    {
        $errorList = $model->getFirstErrors();
        if (empty($errorList)){
            return "";
        }
        if (is_array($errorList)){
            return implode(PHP_EOL, $errorList);
        }
        return "";
    }


    public static function calculatePageCount(int $count, int $pageSize): int
    {
        return intdiv($count, $pageSize) + (($count % $pageSize) ? 1 : 0);
    }
}
