<?php

namespace common\models;

use common\components\Utility;
use yii\behaviors\TimestampBehavior;

/**
 * @property int $id
 * @property string $name
 * @property string $date_registration [datetime]
 *
 * @property Record[] $records
 */
class Reader extends \yii\db\ActiveRecord
{
    public static function tableName(): string
    {
        return 'reader';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'date_created',
                'value' => Utility::getDateNow(),
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['date_created'], 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'date_created' => 'Дата создания',
        ];
    }

    public static function find(): query\ReaderQuery
    {
        return new \common\models\query\ReaderQuery(get_called_class());
    }

    public function getRecords(): \yii\db\ActiveQuery
    {
        return $this->hasMany(Record::class, ['reader_id' => 'id']);
    }
}
