<?php

namespace common\models\query;

use common\models\Record;

/**
 * @see \common\models\Record
 */
class RecordQuery extends \yii\db\ActiveQuery
{

    /**
     * @return Record[]|null
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @return Record|null
     */
    public function one($db = null): ?Record
    {
        return parent::one($db);
    }
}
