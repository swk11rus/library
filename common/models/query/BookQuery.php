<?php

namespace common\models\query;

use common\models\Book;

/**
 * @see \common\models\Book
 */
class BookQuery extends \yii\db\ActiveQuery
{

    /**
     * @return Book[]|null
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @return Book|null
     */
    public function one($db = null): ?Book
    {
        return parent::one($db);
    }
}
