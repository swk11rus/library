<?php

namespace common\models\query;

use common\models\Reader;

/**
 * @see \common\models\Reader
 */
class ReaderQuery extends \yii\db\ActiveQuery
{
    /**
     * @return Reader[]|null
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * @return Reader|null
     */
    public function one($db = null): ?Reader
    {
        return parent::one($db);
    }
}
