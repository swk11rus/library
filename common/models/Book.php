<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * @property int $id
 * @property string $author
 * @property string $title
 * @property string $nickname
 *
 * @property Record[] $records
 */
class Book extends \yii\db\ActiveRecord
{
    public static function tableName(): string
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['author', 'title', 'nickname'], 'required'],
            [['nickname'], 'unique'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'author' => 'Автор',
            'title' => 'Название',
            'nickname' => 'Псевдоним',
        ];
    }

    public function fields(): array
    {
        $fields = parent::fields();
//
//        if ($this->records) {
//            $fields['records'] = function (Book $model) {
//                return $model->records;
//            };
//        }
//        return ArrayHelper::merge(parent::fields(), [
//         'records' => function (Book $model) {
//            return $model->records;
//         }
//        ]);
//        return ArrayHelper::merge($this->fields(), ['records']);

        return $fields;
    }

    public function extraFields()
    {
        $extraFields = parent::extraFields();

        if ($this->records) {
            $extraFields['records'] = function (Book $model) {
                return $model->records;
            };
        }

        return $extraFields;
    }

//    public function extraFields(): array
//    {
//        return ['records'];
//    }

    public static function find(): query\BookQuery
    {
        return new \common\models\query\BookQuery(get_called_class());
    }

    public function getRecords(): \yii\db\ActiveQuery
    {
        return $this->hasMany(Record::class, ['book_id' => 'id']);
    }
}
