<?php

namespace common\models;

use common\components\Utility;
use yii\behaviors\TimestampBehavior;

/**
 * @property int $id
 * @property int $reader_id
 * @property int $book_id
 * @property string $date_issued
 * @property string $date_expected_return
 * @property string $date_returned
 *
 * @property Book $book
 * @property Reader $reader
 */
class Record extends \yii\db\ActiveRecord
{
    public static function tableName(): string
    {
        return 'record';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'date_issue',
                'value' => Utility::getDateNow(),
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['reader_id', 'book_id', 'date_expected_return'], 'required'],
            [['reader_id', 'book_id'], 'integer'],
            [['date_issued', 'date_expected_return', 'date_returned'], 'safe'],
            ['reader_id', 'validateAvailability']
        ];
    }

    public function validateAvailability($attribute)
    {
        $notReturned = static::find()->where([
            'book_id' => $this->book_id,
            'date_returned' => null
        ])->one();

        if ($notReturned !== null) {
            $this->addError($attribute, 'Данную книгу еще не вернули');
        }
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'reader_id' => 'Автор',
            'book_id' => 'Название',
            'date_issued' => 'Дата выдачи',
            'date_expected_return' => 'Дата предполагаемого возврата',
            'date_returned' => 'Дата возврата',
        ];
    }

    public static function find(): query\RecordQuery
    {
        return new \common\models\query\RecordQuery(get_called_class());
    }

    public function getBook(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Book::class, ['id' => 'book_id']);
    }

    public function getReader(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Reader::class, ['id' => 'service_center_id']);
    }
}
