<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // https://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%book}}', [
            'id' => $this->primaryKey(),
            'author' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'nickname' => $this->string()->notNull()->unique(),
        ], $tableOptions);

        $this->createTable('{{%reader}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'date_registration' => $this->date()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%record}}', [
            'id' => $this->primaryKey(),
            'reader_id' => $this->integer()->notNull(),
            'book_id' => $this->integer()->notNull(),
            'date_issued' => $this->date()->notNull(),
            'date_expected_return' => $this->date(),
            'date_returned' => $this->date(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-record-reader_id',
            'record',
            'reader_id',
            'reader',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-record-book_id',
            'record',
            'book_id',
            'book',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-record-reader_id', 'record');
        $this->dropForeignKey('fk-record-book_id', 'record');

        $this->dropTable('{{%book}}');
        $this->dropTable('{{%reader}}');
        $this->dropTable('{{%record}}');
    }
}
