<?php

namespace api\modules\v1\controllers;

use api\base\RestService;
use common\components\Utility;
use common\exceptions\ModelNotValidateException;
use common\models\Reader;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class ReadersController extends RestController
{
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();

        $behaviors = ArrayHelper::merge($behaviors, [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST', 'PUT', 'DELETE'],
                ],
            ],
        ]);

        return $behaviors;
    }

    /**
     * @throws ModelNotValidateException
     */
    public function actionIndex($id = null)
    {
        $request = Yii::$app->request;
        $restService = new RestService(Reader::class);

        if ($id) {
            return $this->view($id);
        }

        if ($request->isGet) {
            return $this->getList();
        }

        if ($request->isPost) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_CREATED;
            return $restService->create();
        }

        if ($request->isPut) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_CREATED;
            return $restService->update();
        }

        if ($request->isDelete) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_ACCEPTED;
            return $restService->delete();
        }
    }

    private function getList(): array
    {
        $request = Yii::$app->request;

        $offset = (int)$request->get('offset') ?: static::DEFAULT_OFFSET;
        $page = (int)$request->get('page') ?? static::DEFAULT_PAGE;
        $expand = $request->get('expand');

        $name = $request->get('name');
        $dateReturned = $request->get('date_returned');

        $query = Reader::find();

        if ($expand) {
            $query->with($expand);
        }

        $query->joinWith('records');
        $query
            ->andFilterWhere(['=', 'reader.name', $name])
            ->andFilterWhere(['=', 'record.date_returned', $dateReturned]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $offset,
                'page' => $page,
            ]
        ]);

        return ArrayHelper::merge([
            'count' => $query->count(),
            'pageCount' => Utility::calculatePageCount($query->count(), $offset),
            'currentPage' => $page,
        ], $dataProvider->getModels());
    }

    private function view(int $id): ?Reader
    {
        $expand = Yii::$app->request->get('expand');

        $query = Reader::find();

        if ($expand) {
            $query->with($expand);
        }

        $query->andWhere(['id' => $id]);

        return $query->one();
    }
}
