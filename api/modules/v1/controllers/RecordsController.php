<?php

namespace api\modules\v1\controllers;

use api\base\RestService;
use common\components\Utility;
use common\exceptions\ModelNotValidateException;
use common\models\Record;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class RecordsController extends RestController
{

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();

        $behaviors = ArrayHelper::merge($behaviors, [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST', 'PUT', 'DELETE'],
                ],
            ],
        ]);

        return $behaviors;
    }

    /**
     * @throws ModelNotValidateException
     */
    public function actionIndex($id = null)
    {
        $request = Yii::$app->request;
        $restService = new RestService(Record::class);

        if ($id) {
            return $restService->view($id);
        }

        if ($request->isGet) {
            return $this->getList();
        }

        if ($request->isPost) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_CREATED;
            return $restService->create();
        }

        if ($request->isPut) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_CREATED;
            return $restService->update();
        }

        if ($request->isDelete) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_ACCEPTED;
            return $restService->delete();
        }
    }

    private function getList(): array
    {
        $request = Yii::$app->request;

        $offset = (int)$request->get('offset') ?: static::DEFAULT_OFFSET;
        $page = (int)$request->get('page') ?? static::DEFAULT_PAGE;
        $expand = $request->get('expand');

        $name = $request->get('name');
        $title = $request->get('title');

        $query = Record::find();

        if ($expand) {
            $query->with($expand);
        }

        $query->joinWith('reader');
        $query->joinWith('book');

        $query
            ->andFilterWhere(['like', 'reader.name', $name])
            ->andFilterWhere(['like', 'book.title', $title]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $offset,
                'page' => $page,
            ]
        ]);

        return ArrayHelper::merge([
            'count' => $query->count(),
            'pageCount' => Utility::calculatePageCount($query->count(), $offset),
            'currentPage' => $page,
        ], $dataProvider->getModels());
    }
}
