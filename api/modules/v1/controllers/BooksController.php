<?php

namespace api\modules\v1\controllers;

use api\base\RestService;
use common\components\Utility;
use common\exceptions\ModelNotFoundException;
use common\exceptions\ModelNotValidateException;
use common\models\Book;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class BooksController extends RestController
{
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();

        $behaviors = ArrayHelper::merge($behaviors, [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST', 'PUT', 'DELETE'],
                ],
            ],
        ]);

        return $behaviors;
    }

    /**
     * @param null $id
     * @return array|mixed|void
     * @throws ModelNotFoundException
     * @throws ModelNotValidateException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionIndex($id = null)
    {
        $request = Yii::$app->request;
        $restService = new RestService(Book::class);

        if ($id) {
            return $restService->view($id);
        }

        if ($request->isGet) {
            return $this->getList();
        }

        if ($request->isPost) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_CREATED;
            return $restService->create();
        }

        if ($request->isPut) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_CREATED;
            return $restService->update();
        }

        if ($request->isDelete) {
            Yii::$app->response->statusCode = static::HTTP_STATUS_ACCEPTED;
            return $restService->delete();
        }
    }

    private function getList(): array
    {
        $request = Yii::$app->request;

        $offset = (int)$request->get('offset') ?: static::DEFAULT_OFFSET;
        $page = (int)$request->get('page') ?? static::DEFAULT_PAGE;
        $expand = $request->get('expand');
        $nickname = $request->get('nickname');
        $dateIssued = $request->get('date_issued');
        $dateReturned = $request->get('date_returned');

        $query = Book::find();

        if ($expand) {
            $query->with($expand);
        }

        $query->joinWith('records');
        $query
            ->andFilterWhere(['like', 'book.nickname', $nickname])
            ->andFilterWhere(['=', 'record.date_issued', $dateIssued])
            ->andFilterWhere(['=', 'record.date_returned', $dateReturned]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $offset,
                'page' => $page,
            ]
        ]);

        return ArrayHelper::merge([
            'count' => $query->count(),
            'pageCount' => Utility::calculatePageCount($query->count(), $offset),
            'currentPage' => $page,
        ], $dataProvider->getModels());
    }
}
