<?php

namespace api\base;

use common\exceptions\ModelNotFoundException;
use common\exceptions\ModelNotValidateException;
use Yii;

class RestService
{
    private string $className;

    public function __construct($className)
    {
        $this->className = $className;
    }

    /**
     * @throws ModelNotValidateException
     */
    public function create()
    {
        $data = Yii::$app->request->post();
        $model = new $this->className();
        $model->setAttributes($data);

        if (!$model->save()) {
            throw new ModelNotValidateException($model);
        }

        return $model;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws ModelNotFoundException
     */
    public function update()
    {
        $params = Yii::$app->request->getBodyParams();

        $model = $this->className::findOne(['id' => $params['id']]);

        if (!$model) {
            throw new ModelNotFoundException('Книга не существует');
        }

        $model->setAttributes($params);

        if (!$model->save()) {
            throw new ModelNotValidateException($model);
        }

        return $model;
    }

    /**
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws ModelNotFoundException
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        $params = Yii::$app->request->getBodyParams();

        $model = $this->className::findOne(['id' => $params['id']]);

        if (!$model) {
            throw new ModelNotFoundException(sprintf('%s не существует', $this->className));
        }

        if (!$model->delete()) {
            throw new ModelNotValidateException($model);
        }

        return $model;
    }

    public function view($id)
    {
        return $this->className::findOne(['id' => $id]);
    }
}